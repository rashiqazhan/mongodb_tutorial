const assert = require("assert");
const mongoose = require("mongoose");
const Author = require("../models/author");

describe("Nesting functions", function () {
  // Create an author with a book
  it("Create an author with sub-documents", function (done) {
    var pat = new Author({
      name: "Patrick Rothfus",
      books: [
        {
          title: "Name of the Wind",
          pages: 400,
        },
      ],
    });
    pat.save().then(function () {
      Author.findOne({ name: "Patrick Rothfus" }).then(function (result) {
        assert(result.books.length === 1);
        console.log("Author created.");
        done();
      });
    });
  });

  // Add a book to an existing author
  it("Adds a book to an author.", function (done) {
    var pat = new Author({
      name: "Patrick Rothfus",
      books: [
        {
          title: "Name of the Wind",
          pages: 400,
        },
      ],
    });
    pat.save().then(function () {
      Author.findOne({ name: "Patrick Rothfus" }).then(function (result) {
        result.books.push({ title: "Wise Man's Fear", pages: 500 });
        result.save().then(function () {
          Author.findOne({ name: "Patrick Rothfus" }).then(function (result) {
            assert(result.books.length === 2);
            done();
          });
        });
      });
    });
  });
});
