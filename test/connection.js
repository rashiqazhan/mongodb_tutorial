const mongoose = require("mongoose");

// Connect to db before beginning test
before(function (done) {
  mongoose.connect("mongodb://localhost:27017/test").then(
    () => {
      console.log("Connection to DB successful."); // execute on successful connection
      done();
    },
    (error) => {
      console.log(error); // display if error if connection fails
    }
  );
});

// Drop collection before each test
beforeEach(function (done) {
  mongoose.connection.collections.mariochars.drop(function () {
    console.log("Collection dropped.");
    done();
  });
});

beforeEach(function (done) {
  mongoose.connection.collections.authors.drop(function () {
    console.log("Collection dropped.");
    done();
  });
});
