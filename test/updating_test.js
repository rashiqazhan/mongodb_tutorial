const assert = require("assert");
const MarioChar = require("../models/mariochar");

describe("Updating Records", function () {
  var character;

  // Insert the data to be updated
  beforeEach(function (done) {
    character = new MarioChar({
      name: "Mario",
      weight: 50,
    });
    character.save(done);
  });

  // Update a single document matching the query
  it("Updates one record from the database", function (done) {
    findQuery = { name: "Mario" };
    updateQuery = { name: "Luigi" };
    MarioChar.findOneAndUpdate(findQuery, updateQuery).then(function () {
      MarioChar.findOne({ _id: character._id }).then(function (result) {
        assert((result.name = updateQuery.name));
        done();
      });
    });
  });

  it("Increments the weight by 1", function (done) {
    MarioChar.updateMany({}, { $inc: { weight: +1 } }).then(function () {
      MarioChar.findOne({ name: "Mario" }).then(function (result) {
        assert(result.weight === 51);
        done();
      });
    });
  });
});
