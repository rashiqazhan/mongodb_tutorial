const assert = require("assert");
const MarioChar = require("../models/mariochar");

describe("Saving records", function () {
  it("Saves a record to the database", function (done) {
    var character = new MarioChar({
      name: "Mario",
    });
    character.save().then(function(){
      assert(character.isNew === false);
      done();
    });
  });
});
