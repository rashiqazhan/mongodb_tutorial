const assert = require("assert");
const MarioChar = require("../models/mariochar");

describe("Finding Records", function () {
  var character;

  // Insert the data to be queried
  beforeEach(function (done) {
    character = new MarioChar({
      name: "Mario",
    });
    character.save(done);
  });

  // Find a single document matching the query
  it("Finds one record from the database", function (done) {
    query = { name: "Mario" };
    MarioChar.findOne(query).then(function (result) {
      assert(result.name === query.name);
      done();
    });
  });

  // Find a single document matching the objectId
  it("Finds one record by _id from the database", function (done) {
    query = { _id: character._id };
    MarioChar.findOne(query).then(function (result) {
      assert(result._id.toString() === character._id.toString());
      done();
    });
  });
});
