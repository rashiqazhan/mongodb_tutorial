const assert = require("assert");
const MarioChar = require("../models/mariochar");

describe("Deleting Records", function () {
  var character;

  // Insert the data to be deleted
  beforeEach(function (done) {
    character = new MarioChar({
      name: "Mario",
    });
    character.save(done);
  });

  // Delete a single document matching the query
  it("Deletes one record from the database", function (done) {
    query = { name: "Mario" };
    MarioChar.findOneAndRemove(query).then(function () {
      MarioChar.findOne(query).then(function (result) {
        assert(result === null);
        done();
      });
    });
  });
});
