const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// schema
const BookSchema = new Schema({
    title: String,
    page: Number,
  });
  
const AuthorSchema = new Schema({
  name: String,
  age: Number,
  books: [BookSchema],
});

//model
const Author = mongoose.model("author", AuthorSchema);

module.exports = Author;
